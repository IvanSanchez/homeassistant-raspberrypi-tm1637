"""Config flow for TM1637"""
from __future__ import annotations

from homeassistant.data_entry_flow import FlowResult
from homeassistant import config_entries

# import homeassistant.helpers.config_validation as cv
import voluptuous as vol
import logging
from .const import DOMAIN
from homeassistant.helpers.selector import (
    EntitySelector,
    NumberSelector,
    NumberSelectorConfig,
)

_LOGGER = logging.getLogger(__name__)


class TM1637ConfigFlow(config_entries.ConfigFlow, domain=DOMAIN):
    VERSION = 1

    CONNECTION_CLASS = config_entries.CONN_CLASS_LOCAL_PUSH

    async def async_step_user(self, user_input) -> FlowResult:
        """Handle the user-driven step: ask the user for an entity."""

        if not user_input:
            return self._show_form_user()

        # matching_items = [clear_name(i) for i in matching_items]
        _LOGGER.debug("creating entry for", user_input)
        clk = int(user_input["clk"])
        dio = int(user_input["dio"])
        return self.async_create_entry(
            title=f"TM1637 {clk}/{dio}",
            data={
                "entity": user_input["entity"],
                "clk": clk,
                "dio": dio,
            },
        )

    def _show_form_user(
        self,
        user_input: dict[str, Any] | None = None,
        errors: dict[str, Any] | None = None,
    ) -> FlowResult:
        if user_input is None:
            user_input = {}
        return self.async_show_form(
            step_id="user",
            data_schema=vol.Schema(
                {
                    vol.Required("entity"): EntitySelector(),
                    vol.Required("clk", default=5): NumberSelector(
                        NumberSelectorConfig(min=1, max=99, step=1)
                    ),
                    vol.Required("dio", default=4): NumberSelector(
                        NumberSelectorConfig(min=1, max=99, step=1)
                    ),
                }
            ),
            errors=errors,
        )
