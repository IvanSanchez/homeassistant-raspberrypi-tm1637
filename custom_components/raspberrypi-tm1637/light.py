"""Light platform for TM1637."""
from __future__ import annotations

from homeassistant.components.light import (
    ATTR_BRIGHTNESS,
    ColorMode,
    LightEntity,
    LightEntityFeature,
)
from homeassistant.core import HomeAssistant
from homeassistant.helpers.entity_platform import AddEntitiesCallback
from homeassistant.helpers.typing import ConfigType, DiscoveryInfoType
from homeassistant.helpers.entity import EntityCategory
from homeassistant.helpers.event import TrackStates

from homeassistant.const import EVENT_STATE_CHANGED, EVENT_HOMEASSISTANT_STOP

from .const import DOMAIN
import asyncio

from tm1637 import TM1637
import logging

_LOGGER = logging.getLogger(__name__)


async def async_setup_entry(hass, config_entry, async_add_entities):
    """Add lights for passed config_entry in HA."""

    _LOGGER.info(
        f"Setting up TM1637 Light: {config_entry.data['clk']}/{config_entry.data['dio']} for {config_entry.data['entity']}"
    )

    clk = config_entry.data["clk"]
    dio = config_entry.data["dio"]

    lights = [
        TM1637Light(
            clk=clk,
            dio=dio,
            copy_entity=config_entry.data["entity"],
            hass=hass,
        ),
    ]

    return async_add_entities(lights)


class TM1637Light(LightEntity):
    """An individual TM1637 display."""

    def __init__(
        self, clk=5, dio=4, hass=None, copy_entity=None, device_info=None, **kwargs
    ):
        self._attr_name = f"TM1637 Display {clk}/{dio}"
        self._attr_unique_id = f"tm1637-{clk}-{dio}"
        self._attr_should_poll = False
        self._attr_icon = "mdi:numeric"
        self._attr_device_info = device_info
        self._attr_available = True

        self.hass = hass
        self._shutdown = False
        self._value = None

        self._attr_is_on = True
        self._attr_supported_color_modes = set(ColorMode.BRIGHTNESS)

        # Start with max brightness
        self.tm1637 = TM1637(clk, dio, brightness=7)

        self.copy_entity = copy_entity

        def handle_event(event):
            if event.data["entity_id"] == self.copy_entity:
                self.update_display(event.data["new_state"].state)

        hass.bus.async_listen(EVENT_STATE_CHANGED, handle_event)
        hass.bus.async_listen_once(EVENT_HOMEASSISTANT_STOP, self.on_hass_stop)

        _LOGGER.info(f"{self._attr_name} listening for events")

    @property
    def brightness(self):
        raw_brightness = self.tm1637.brightness()
        _LOGGER.debug(f"Raw brightness: {raw_brightness}")

        # Convert from 0..7 range into 0..255 range
        return 32 * (raw_brightness + 1) - 1

    def turn_on(self, **kwargs):
        _LOGGER.info(f"Turning on: {kwargs}")
        if ATTR_BRIGHTNESS in kwargs:
            if kwargs[ATTR_BRIGHTNESS] == 0:
                self.tm1637_set_brightness(0)
            else:
                brightness = max(int((kwargs[ATTR_BRIGHTNESS] * 100) / 8 - 1), 1)
                self.tm1637_set_brightness(brightness)
        self._attr_is_on = True
        self.update_display(self._value)
        self.async_write_ha_state()

    def turn_off(self, **kwargs):
        _LOGGER.info(f"Turning off: {kwargs}")
        self.tm1637_set_brightness(0)
        self.tm1637.show("    ")
        self._attr_is_on = False
        self.async_write_ha_state()

    async def tm1637_set_brightness(self, b):
        _LOGGER.debug(f"Setting brightness level: {b}")
        asyncio.create_task(self.tm1637.brightness(b))

    def update_display(self, value):
        _LOGGER.debug(f"Updating screen: {value}")
        self._value = value
        if self._shutdown:
            return
        if not self.is_on:
            self.tm1637.show("    ")
        elif value is None or value == "unavailable":
            self.tm1637.show("----")
        else:
            self.tm1637.number(int(float(value)))

    def async_remove(self, **kwargs):
        self._shutdown = True
        self.tm1637.show("----")
        return super().async_remove(**kwargs)

    def on_hass_stop(self, event, **kwargs):
        self._shutdown = True
        self.tm1637.show("----")
