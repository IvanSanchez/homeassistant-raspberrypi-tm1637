
Displays the value of a Home Assistant entity in a TM1637 display, by means of the https://[`raspberrypi-tm1637` python library](github.com/depklyon/raspberrypi-tm1637/).

See [https://freeds.es/](https://freeds.es/) for more information about the devices themselves, and see [https://gitlab.com/IvanSanchez/homeassistant-raspberrypi-tm1637](https://gitlab.com/IvanSanchez/homeassistant-raspberrypi-tm1637) for more information about this integration.
