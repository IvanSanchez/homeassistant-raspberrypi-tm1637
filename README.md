# homeassistant-raspberrypi-tm1637

Displays the value of a Home Assistant entity in a TM1637 display, by means of the [`raspberrypi-tm1637` python library](https://github.com/depklyon/raspberrypi-tm1637/).

![](homeassistant-raspberrypi-tm1637.webp)

Note that this needs Home Assistant to be running on a Raspberry Pi (or a similar single-board computer). In other words: the TM1637s must be connectd to GPIO pins on the **same** computer that is running Home Assistant.

## Installation

#### Guided installation

- Enable the [HACS](https://hacs.xyz/) integration in your Home Assistant instance.
- Use the side menu to browse HACS.
- Navigate to "Integrations", then use the overflow menu (three dots at the top-left) to add a Custom Repository.
- Enter the URL `https://github.com/IvanSanchez/homeassistant-raspberrypi-tm1637`, of type "Integration"
- You should see a new box labelled "DGT InfoCar". Click on it and follow HACS' instructions to download and enable the integration.
- Restart Home Assistant when HACS tells you to.

#### Manual installation

Download the files from this repository. Copy the `custom_components/raspberrypi-tm1637/` directory into the `custom_components` directory of your Home Assistant instance.

e.g. if your configuration file is in `/home/homeassistant/.homeassistant/configuration.yaml`, then the files from this integration should be copied to `/home/homeassistant/.homeassistant/custom_components/raspberrypi-tm1637/`.

Restart Home Assistant to ensure the integration can be detected.

## Usage

Use the Home Assistant GUI to add a new integration (settings → devices & services → add new integration). You should find the `raspberrypi-tm1637` integration in the list.

Select which entity should have its value displayed in the TM1637 display, and which GPIO pins are used for `CLK` and `DIO`. The display will show up as a light in Home Assistant, allowing you to turn it on/off.

If you need to change the entity being displayed, delete the integration and add it again.

## Limitations

* This has been tested on a Raspberry Pi. It *should* work on similar single-board computers (Pine64, banana pi, orange pi, olimex, etc) but has not been tested.

* This depends on the [`raspberrypi-tm1637`](https://pypi.org/project/raspberrypi-tm1637/) python library - restrictions from that library apply.

* It should be possible to control the brightness of the display (by means of controlling the brightness of the Home Assistant light), but that's buggy at the moment.

## Bugs? Comments?

Use the gitlab issue tracker at https://gitlab.com/IvanSanchez/homeassistant-raspberrypi-tm1637/-/issues

(Yes, it's Git**Lab** and not Git**Hub**. Development happens at GitLab. The GitHub repo is only for HACS compatibility.)

Please keep in mind that it's an issue tracker, and not a discussion forum. I recommend reading ["How to Report Bugs Effectively"](https://www.chiark.greenend.org.uk/~sgtatham/bugs.html) if you've never written into an issue tracker before.

## License

Licensed under GPLv3. See the `LICENSE` file for details.
